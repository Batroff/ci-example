package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/batroff/ci-example/pkg/env"
)

const (
	dbPswdEnv  = "POSTGRES_PSWD"
	appNameEnv = "APP_NAME"
	charEnv    = "CHAR"
)

func main() {
	task1()
	task2()
}

func task1() {
	ch, err := env.Lookup(charEnv)
	if err != nil {
		log.Printf("environment key '%s' not found", charEnv)
		return
	}

	f, err := os.Create("output/output_1")
	if err != nil {
		log.Printf("create artifact")
		return
	}
	defer f.Close()

	i, err := strconv.Atoi(ch)
	if err != nil {
		_, _ = fmt.Fprintln(f, "TASK 1: not integer")
		return
	}

	_, _ = fmt.Fprintf(f, "TASK 1: %d\n", i)
}

type Subject struct {
	Name string
	Cnt  int
}

func task2() {
	dbPswd, err := env.Lookup(dbPswdEnv)
	if err != nil {
		log.Fatalln(err)
	}

	dbPool, err := pgxpool.Connect(context.Background(), fmt.Sprintf("postgres://postgres:%s@ci_db:5432/postgres", dbPswd))
	if err != nil {
		log.Fatalf("connect to database: %v", err)
	}
	defer dbPool.Close()

	rows, err := dbPool.Query(context.Background(), "select sub.name, COUNT(st.id) from subjects sub join students st on st.id = sub.student_id GROUP BY sub.name;")
	if err != nil {
		log.Println("query err:", err)
		return
	}

	subjects := make([]Subject, 0)

	for rows.Next() {
		var subj Subject
		if err = rows.Scan(&subj.Name, &subj.Cnt); err != nil {
			log.Println("scan err:", err)
			return
		}

		subjects = append(subjects, subj)
	}

	f, err := os.Create("output/output_2")
	if err != nil {
		log.Printf("create artifact")
		return
	}
	defer f.Close()

	const row = "'%s': %d\n"

	for _, subj := range subjects {
		_, _ = fmt.Fprintf(f, row, subj.Name, subj.Cnt)
	}
}
