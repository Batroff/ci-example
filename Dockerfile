FROM golang:1.17.8-alpine3.15

COPY . /app

# root app dir
WORKDIR /app

RUN mkdir -p ./bin && \
    go build -o ./bin/ ./cmd/main.go

RUN mkdir -p ./output

CMD ["/app/bin/main"]