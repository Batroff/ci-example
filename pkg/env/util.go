package env

import (
	"errors"
	"fmt"
	"log"
	"os"
)

var (
	ErrEnvVarNotFound = errors.New("environment variable not found")
)

func Lookup(name string) (string, error) {
	val, ok := os.LookupEnv(name)
	if !ok {
		return "", fmt.Errorf("looking for env '%s': %w", name, ErrEnvVarNotFound)
	}

	return val, nil
}

func LookupDefault(name, defaultVal string) string {
	v, err := Lookup(name)
	if err != nil {
		log.Printf("%s: set default %s", err, defaultVal)
		return defaultVal
	}

	return v
}
