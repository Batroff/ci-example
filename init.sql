DROP TABLE IF EXISTS subjects;
DROP TABLE IF EXISTS students;

CREATE TABLE students
(
    id       UUID,
    name     text,
    birthday date,
    PRIMARY KEY (id)
);

CREATE TABLE subjects
(
    id         UUID,
    name       text,
    exam_date  date,
    teacher    text,
    mark       int,
    student_id UUID,
    PRIMARY KEY (id),
    FOREIGN KEY (student_id) REFERENCES students
);

INSERT INTO students(id, name, birthday) VALUES('d74953a5-1018-4733-93e9-a2d6dcc7dc47', 'Samantha Anderson', '2005-09-20');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('597b554a-ec3d-4117-bbc3-63aa73ff18ea', 'exploit turn-key schemas', '1994-10-11', 'Robert Sutton', 4, 'd74953a5-1018-4733-93e9-a2d6dcc7dc47');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('719f7253-a872-425f-8bf9-53cec3bbb682', 'redefine holistic web-readiness', '2004-09-16', 'Jacob Mcneil', 4, 'd74953a5-1018-4733-93e9-a2d6dcc7dc47');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('d9584301-88d6-4556-80d3-3b5ad57147d7', 'exploit turn-key schemas', '1983-09-19', 'Kyle Salas', 4, 'd74953a5-1018-4733-93e9-a2d6dcc7dc47');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('0222234d-53ea-41de-9bac-02828f2a00d9', 'redefine holistic web-readiness', '2021-11-03', 'Robert Jones', 4, 'd74953a5-1018-4733-93e9-a2d6dcc7dc47');
INSERT INTO students(id, name, birthday) VALUES('89cce4b8-4455-4143-9de8-dbbf7c0b6972', 'Kelsey Blackburn', '2013-03-01');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('b49f68d9-c8f6-4fa2-9c1a-41e9e6c1fff1', 'redefine holistic web-readiness', '2016-04-04', 'Nicole Strong', 4, '89cce4b8-4455-4143-9de8-dbbf7c0b6972');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('2d3fcbae-a4cb-4058-ade5-7344a24c930c', 'embrace leading-edge infrastructures', '1982-11-29', 'David Sanchez', 4, '89cce4b8-4455-4143-9de8-dbbf7c0b6972');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('f52675f1-c73c-45df-9ae8-64f3400d9a41', 'drive cutting-edge platforms', '1979-11-10', 'Lindsey Ellis', 4, '89cce4b8-4455-4143-9de8-dbbf7c0b6972');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('5005b935-c13e-4460-b1fb-26907615a79a', 'integrate frictionless networks', '1979-11-30', 'Laura Barker', 4, '89cce4b8-4455-4143-9de8-dbbf7c0b6972');
INSERT INTO students(id, name, birthday) VALUES('50ac1614-5943-4e7d-8087-988b532beb4d', 'Robert Fuller', '1990-06-29');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('0e24ac46-9738-4364-9dce-3cb5d3686ad3', 'integrate frictionless networks', '1990-04-29', 'Mr. Jonathan Elliott', 2, '50ac1614-5943-4e7d-8087-988b532beb4d');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('cb811292-fd5b-4c49-8252-734dd102dc98', 'exploit turn-key schemas', '2007-06-07', 'John Curtis', 3, '50ac1614-5943-4e7d-8087-988b532beb4d');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('95bc712b-ff41-4d93-aa90-102bc51b32b3', 'drive cutting-edge platforms', '2001-02-16', 'Jeffrey Sanchez', 3, '50ac1614-5943-4e7d-8087-988b532beb4d');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('25951309-5b91-44f8-992a-68802065f3aa', 'integrate frictionless networks', '2004-08-11', 'Grant Wheeler', 4, '50ac1614-5943-4e7d-8087-988b532beb4d');
INSERT INTO students(id, name, birthday) VALUES('444ad6f9-59d3-4283-b087-fc3c05221cd6', 'Amber Mckay', '1984-02-20');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('8d8764ae-27ad-42d1-91c6-1da539bd4290', 'engineer intuitive relationships', '1985-10-28', 'Heidi Smith', 2, '444ad6f9-59d3-4283-b087-fc3c05221cd6');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('1a66311c-3791-4b5d-879f-51b8b1912032', 'redefine holistic web-readiness', '2019-01-20', 'Jacob Hubbard', 5, '444ad6f9-59d3-4283-b087-fc3c05221cd6');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('2fce39e2-20ae-46bb-9c95-6c5c05abfc10', 'integrate frictionless networks', '2008-07-16', 'Michele Haley', 2, '444ad6f9-59d3-4283-b087-fc3c05221cd6');
INSERT INTO students(id, name, birthday) VALUES('f36a978d-f1a3-4b46-a08f-af569dde7acd', 'Jesse Patterson', '1996-07-07');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('9fc64544-d846-4260-8cb2-5fcb9cec893b', 'embrace leading-edge infrastructures', '2012-03-25', 'Megan Hill', 2, 'f36a978d-f1a3-4b46-a08f-af569dde7acd');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('91446334-8b83-4071-9d57-a7a11b037593', 'drive cutting-edge platforms', '2009-10-21', 'Phillip Bryant', 3, 'f36a978d-f1a3-4b46-a08f-af569dde7acd');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('4c5575b5-e080-4352-a2c7-d846e3c18ede', 'integrate frictionless networks', '2020-07-20', 'Christina Wilson', 5, 'f36a978d-f1a3-4b46-a08f-af569dde7acd');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('646c19e6-4a12-4419-8a2c-0f141aa35f5f', 'integrate frictionless networks', '1974-10-01', 'Jonathan Valenzuela', 4, 'f36a978d-f1a3-4b46-a08f-af569dde7acd');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('4236af3c-c6e2-41f2-adeb-7051c2f38e97', 'drive cutting-edge platforms', '1974-12-17', 'Susan Little', 5, 'f36a978d-f1a3-4b46-a08f-af569dde7acd');
INSERT INTO students(id, name, birthday) VALUES('e3d00506-e969-4493-a624-ad239e576d78', 'Robert Holland', '1977-03-15');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('4248757e-c897-4f53-b957-d835b24f620e', 'integrate frictionless networks', '1976-10-27', 'Timothy Allen', 4, 'e3d00506-e969-4493-a624-ad239e576d78');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('40629d00-e683-4b9a-9d2c-94ab543e513a', 'engineer intuitive relationships', '2009-07-20', 'Roger Smith', 2, 'e3d00506-e969-4493-a624-ad239e576d78');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('f3835754-1a06-47f5-a08e-7436b450dae9', 'drive cutting-edge platforms', '1993-10-10', 'Jessica Simpson', 4, 'e3d00506-e969-4493-a624-ad239e576d78');
INSERT INTO students(id, name, birthday) VALUES('c225db55-1b2f-4a76-b614-9ce2eea5fcab', 'Sierra Phillips', '1997-09-23');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('68298d3a-c8f7-4195-a633-e20fffadb07c', 'redefine holistic web-readiness', '1998-12-08', 'Angela Ingram', 2, 'c225db55-1b2f-4a76-b614-9ce2eea5fcab');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('87f2e52b-7d17-4683-b526-d533190475eb', 'engineer intuitive relationships', '1986-10-27', 'Cynthia Barr', 4, 'c225db55-1b2f-4a76-b614-9ce2eea5fcab');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('730e205b-ae98-4e91-a536-7f09d1fabc63', 'exploit turn-key schemas', '2004-06-05', 'Lynn Gonzalez', 4, 'c225db55-1b2f-4a76-b614-9ce2eea5fcab');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('64758bad-bc12-4174-82d9-02d757b42382', 'exploit turn-key schemas', '2021-09-10', 'Maxwell Rodriguez', 3, 'c225db55-1b2f-4a76-b614-9ce2eea5fcab');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('f38d08da-1a1f-4ea1-994c-70fac45dca47', 'embrace leading-edge infrastructures', '2005-12-25', 'Kevin Wilson', 4, 'c225db55-1b2f-4a76-b614-9ce2eea5fcab');
INSERT INTO students(id, name, birthday) VALUES('28f67c51-63fb-4109-9b48-57bd17ee6a0e', 'Hunter Ayala', '1985-04-13');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('d1792f89-0f71-40c9-84f7-7227007e379b', 'engineer intuitive relationships', '2012-01-20', 'Kellie Leon', 2, '28f67c51-63fb-4109-9b48-57bd17ee6a0e');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('0ba630b9-3a30-4a0e-bc0e-8891544888eb', 'drive cutting-edge platforms', '1994-09-29', 'Karen Young', 5, '28f67c51-63fb-4109-9b48-57bd17ee6a0e');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('721ec6d5-b704-44bb-9e96-d284ba53e66d', 'integrate frictionless networks', '2017-09-30', 'David Watson', 3, '28f67c51-63fb-4109-9b48-57bd17ee6a0e');
INSERT INTO students(id, name, birthday) VALUES('fadc7a33-d4e1-4c3a-9e73-0b58d904fec5', 'Jack Bradford', '1986-02-23');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('c47e00ac-0d09-4247-b293-f259b82f870a', 'drive cutting-edge platforms', '1998-10-10', 'Nicole Walker', 3, 'fadc7a33-d4e1-4c3a-9e73-0b58d904fec5');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('d473480b-3e63-4870-8f61-73431acec507', 'engineer intuitive relationships', '1976-02-20', 'Raymond Leonard', 3, 'fadc7a33-d4e1-4c3a-9e73-0b58d904fec5');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('8d4a6eeb-3584-4135-a519-1152c07249e4', 'engineer intuitive relationships', '1995-06-29', 'John Spencer', 3, 'fadc7a33-d4e1-4c3a-9e73-0b58d904fec5');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('6fca5baf-3012-4ff8-9fe4-f1c642e75c57', 'redefine holistic web-readiness', '1986-08-06', 'Lance Turner DDS', 3, 'fadc7a33-d4e1-4c3a-9e73-0b58d904fec5');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('b86ae1ac-6c49-4d0e-aabd-723dbbe7ce09', 'embrace leading-edge infrastructures', '1990-11-08', 'Mary Lane', 2, 'fadc7a33-d4e1-4c3a-9e73-0b58d904fec5');
INSERT INTO students(id, name, birthday) VALUES('4fbc3137-0b3d-4528-8b48-930915f8f4f9', 'Luis Middleton', '2005-03-31');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('8dff1248-d013-4cd9-8f1e-558abdab4a75', 'exploit turn-key schemas', '1996-10-08', 'Rhonda Bowers', 2, '4fbc3137-0b3d-4528-8b48-930915f8f4f9');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('b0e098e2-2394-4dcb-837b-e99fb77d0cb3', 'embrace leading-edge infrastructures', '1998-02-25', 'Angela Hart', 2, '4fbc3137-0b3d-4528-8b48-930915f8f4f9');
INSERT INTO subjects(id, name, exam_date, teacher, mark, student_id) VALUES('7c28d214-636b-498a-95e0-59df522d7bfa', 'drive cutting-edge platforms', '2020-05-03', 'Michele Francis', 2, '4fbc3137-0b3d-4528-8b48-930915f8f4f9');
